(Toutes les commandes oc doivent être executées depuis le master de votre cluster)

# Mise en place d'Openshift Origin

## Connexion Utilisateur
```
 # oc login -u <user>
```
puis tapez votre mot de passe
## Connexion Admin
```
 # oc login -u system:admin
```
### Ajouter les droits root à votre utilisateur
```
 # oadm policy add-cluster-role-to-user cluster-admin <tosau> <-- user spécifié dans le fichier users.htpasswd
 # oc adm policy add-scc-to-group privileged system:authenticated
 # oc adm policy add-scc-to-group anyuid system:authenticated
```
# Utilisation de GlusterFS

Une StorageClass est créée lors du déploiement du Cluster grâce aux variables spécifiées dans le fichier cluster.conf :
```
openshift_storage_glusterfs_storageclass=true
openshift_storage_glusterfs_storageclass_default=true
```
- On peut vérifier qu'elle existe avec la commande :
```
 # oc get storageclass
```
- Cette Class doit être appelée lors de la création d'un PVC (Persistent Volume Claim) qui sera utilisé par un pod

## Création d'un PVC :
- On créé le fichier **pvc.yaml** qui appelle la StorageClass :

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
 name: gluster
spec:
 accessModes:
  - ReadWriteMany
 resources:
   requests:
        storage: 5Gi
 storageClassName: glusterfs-storage
```
- On créé ensuite le PVC avec la commande :

```
 # oc create -f pvc.yaml
```
- On vérifie que celui-ci à bien été créé et fonctionne bien avec :

```
 # oc get pvc
 # oc describe pvc <name> <-- name spécifié dans le fichier pvc.yaml
```
- La catégorie Events dans le résultat obtenu permet de voir s'il y a eu une erreur

## Rattachement du PVC à un pod :

- On crée le fichier **pod.yaml** qui appelle le PVC :

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    name: nginx-pod
spec:
  containers:
  - name: nginx-pod
    image: nginx
    ports:
    - name: web
      containerPort: 80
    volumeMounts:
    - name: gluster-vol1
      mountPath: /usr/share/nginx/html
      readOnly: false
  volumes:
  - name: gluster-vol1
    persistentVolumeClaim:
      claimName: gluster
```
- On crée ensuite le pod avec la commande :

```
 # oc create -f pod.yaml
```
- On vérifie que celui-ci à bien été créé et fonctionne bien avec :

```
 # oc get pods
 # oc describe pod <name> <-- name spécifié dans le fichier pod.yaml
```

- La catégorie Events dans le résultat obtenu permet de voir s'il y a eu une erreur

- Cette partie sera expliquée avec la console web

# Utilisation de la console

- L'adresse https://<ip_master>:8443 permet d'acceder à la console web
- Connectez-vous ensuite avec votre identifiant et mot de passe défini dans le fichier users.htpasswd appelé dans le fichier de configuration.

- Si vous n'avez pas de projet en cours votre page d'accueil sera le catalogue. Veuillez en créer un en haut à droite.
- Vous arrivez donc sur le Dashboard de votre projet.

## Monitoring

Pour chaque PVC, pod, application, secret ... déployé vous pouvez voir ses "Events" dans l'onglet Monitoring. C'est l'équivalent des logs d'openshift qui vous décrit si votre application ou autre a bien été déployée. Nous allons voir plusieurs exemples durant cette documentation.

## Création d'une Application

- Dérouler le menu "Add to Project" en haut à droite et cliquez sur "Deploy Image"

- Si vous avez déja pull l'image voulu cochez Image Stream Tag et sélectionnez le projet, l'image, et le tag
- Sinon cochez l'autre option et recherchez votre image. 
Par exemple `docker.io/nginx:latest`
- Pensez à scroller la fenêtre si vous voulez spécifier des variables d'environnements ou autre.

- Vous pourrez alors voir votre application dans l'onglet Overview.
- Pour revenir au Monitoring, vous pouvez par exemple ici allez dans l'onglet Applications>Pods puis cliquer sur le pod que l'on vient de créer
- Dans l'onglet "Events" on voit tout ce qu'openshift à fait concernant ce pod. (On y vera également les erreurs pour débuguer)

## Creation d'un PVC :

Vous pouvez importer le fichier **pvc.yaml** (vu au-dessus) dans le menu déroulant Add to Project en haut à droite.
Il sera alors créé et visible dans l'onglet Storage sur la gauche.
Ou alors vous pouvez directement vous rendre dans l'onglet Storage et cliquer sur Create Storage en haut à droite.
			
- Sélectionnez la Storage Class glusterfs-storage (crée lors du déploiement du cluster)
- Donnez lui un nom
- Selectionnez un Access Mode (C'est une règle qui va fixer les permissions d'un volume pour savoir si le pod va pouvoir écrire dessus ou si le volume pourra être montée par plusieurs pods) Pour plus d'informations lisez la [documentation officiel](https://docs.openshift.org/latest/architecture/additional_concepts/storage.html#pv-access-modes)
- Définissez une taille et validé

## Rattachement du PVC à un pod :

- Si vous déployez un pod avec un fichier YAML, vous avez uniquement à appeler le PVC comme dans le fichier **pod.yaml**

- Sinon allez dans l'onglet Overview, cliquez sur l'application qui doit recevoir le PVC
- Déroulez ensuite le menu Action en haut à droite et cliquez sur "Add Storage"

- Sélectionnez d'abord le PVC voulu
- Renseignez le chemin où sera monté le PVC ( ! Attention si le PVC est vide, les données situées à cette endroit sera perdues ! )
- Optionel le chemin à monter du PVC (Sert si le PVC à été partagé)
- Optionel spécifié un nom au volume pour que ce soit plus parlant

L'application va alors redémarrer en montant le volume. Encore une fois en cliquant sur votre pod puis dans l'onglet "Events" vous pourrez voir que votre volume à bien été monté et que le container à démarré.
