
# Monitoring pour Openshift

## Définir variable dans le fichier /etc/ansible/hosts

On met toutes ses variables à la fin du fichier **/etc/ansible/hosts**

```
openshift_metrics_install_metrics=True
openshift_metrics_cassandra_storage_type=pv
openshift_metrics_hawkular_hostname=metrics.example.com
openshift_metrics_cassandra_pvc_size=5
openshift_metrics_image_version=v3.9
openshift_prometheus_namespace=openshift-grafana
```

On définit que l'on souhaite installer metrics, que l'on veut créer un volume persistant pour  la base de données.
On spécifie ensuite l'adresse à laquelle on accède aux données et la taille du volume.
Et pour finir, le namespace de prometheus.

## Installation de Metrics

```
# ansible-playbook openshift-ansible/playbooks/openshift-metrics/config.yml
```

Cela va déployer des pods dans le namespace openshift-infra (cela va mettre beaucoup de temps 5-10 min le temps que la base de données cassandra démarre) 

## Installation de Prometheus

```
# ansible-playbook openshift-ansible/playbooks/openshift-prometheus/config.yml
```

Vous pouvez si vous le souhaitez, éditer les routes du namespace openshift-grafana pour correspondre à vos besoins. Par exemple, changer la route "grafana-openshift-grafana.router.default.svc.cluster.local" en grafana.example.com
Allez sur le lien de votre route prometheus et connectez-vous avec votre compte Openshift pour activez la réception de données.

## Installation de Grafana 

```
# ansible-playbook openshift-ansible/playbooks/openshift-grafana/config.yml
```

## Configurer Grafana

Allez sur le lien de votre route, pour moi https://grafana.example.com, et dans le menu sélectionnez Data Sources, puis prometheus.
Changez ensuite l'url dans la partie HTTP settings par https://prometheus. Faites ensuite Save & Test et vous devez voir "Data source is working".  
Vous pouvez alors ensuite allez dans le menu puis Dashboard. Cliquez ensuite sur Home en haut et changez de Dashboard avec "openshift cluster monitoring"

Vous pouvez alors voir les graphs de sdonnées de vos containeurs et pods.

## Erreurs connus
- Lors de l'installation de Metrics, il se peut que cela ne marche pas car il manque certains paquets python (python2-passlib).
Veuillez donc les installer et recommencez l'installation.


