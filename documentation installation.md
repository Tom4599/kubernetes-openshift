N'hésitez pas à consulter la [documentation officielle d'Openshift Origin](https://docs.okd.io/) si celle-ci n'est pas assez claire. Attention, si vous faite une recherche google, à bien tomber sur la documention d'openshift Origin (non Entreprise) et de vérifier la version d'Openshift dans l'url.
# Installation d'Openshift Origin 3.9
Dans cet exemple nous allons mettre en place un cluster kubernetes de trois machines à l'aide d'Openshift Origin 3.9 et GlusterFS pour le stockage persistant.
Comme Openshift est un produit RedHat, celui-ci n'est compatible que pour les distributions RedHat, nos trois serveurs sont donc ici des CentOS. Pensez à bien respecter [les prérequis définis par RedHat](https://docs.okd.io/3.9/install_config/install/prerequisites.html) pour vos machines. 
## Préparations des serveurs 
**Les commandes suivantes doivent être exécutées sur chaque serveur, y compris le master**

Mettre à jour les paquets :
```
# yum update
``` 
Définir un hostname si c'est pas déjà fait :
```
# hostnamectl set-hostname <your-new-hostname>
```
Vérifier le PATH
```
# echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
```
Générer des clés SSH si c'est pas déjà fait : 
```
# ssh-keygen -t rsa
```
Modifier le fichier /etc/hosts avec les ips de chacunes de vos machines. (Seulement si vos machines ne sont pas joignable directement avec un DNS)
```
# cat /etc/hosts
192.168.56.11   master
192.168.56.12   node1
192.168.56.14   node2
```
Envoyer sa clé publique sur chacune des machines même à sois-même.
```
# ssh-copy-id root@master
# ssh-copy-id root@node1
# ssh-copy-id root@node2
```
Vérifiez ensuite que vous pouvez vous connecter à chacune des machines sans mot de passe.
Installer les paquets nécessaires :
```
# yum -y install wget git net-tools bind-utils yum-utils iptables-services bridge-utils bash-completion kexec-tools sos psacct vim
# reboot
```
Installer le dernier repo EPEL
```
# yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```
Désactiver le repo
```
# sed -i -e "s/^enabled=1/enabled=0/" /etc/yum.repos.d/epel.repo
```
Installer ansible
```
# yum -y --enablerepo=epel install ansible pyOpenSSL
```
Installer docker
```
# yum -y install docker
```
On ajoute ensuite également des disques durs pour GlusterFS (sans table de partition, rien).

## Téléchargement des playbooks et création des fichiers pour ansible (A faire sur le master)
Téléchargement du git avec les playbooks pour Openshift Origin 3.9
```
# cd ~
# git clone -b release-3.9 https://github.com/openshift/openshift-ansible 
# cd openshift-ansible
# git checkout release-3.9
```
Création du fichier cluster.conf 
```
[masters]
master openshift_ip=192.168.56.11

[etcd]
master

[nodes]
master openshift_node_labels="{'region': 'primary','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.11 openshift_schedulable=true
node1 openshift_node_labels="{'region': 'infra','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.12 openshift_schedulable=true
node2 openshift_node_labels="{'region': 'infra','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.14 openshift_schedulable=true

[glusterfs]
master glusterfs_ip=192.168.56.11 glusterfs_devices='["/dev/sdb"]'
node1 glusterfs_ip=192.168.56.12 glusterfs_devices='["/dev/sdb"]'
node2 glusterfs_ip=192.168.56.14 glusterfs_devices='["/dev/sdb"]'

[OSEv3:children]
masters
nodes
etcd
glusterfs

[OSEv3:vars]
openshift_storage_glusterfs_wipe=true
openshift_storage_glusterfs_storageclass=true
openshift_storage_glusterfs_storageclass_default=true

ansible_ssh_user=root

openshift_deployment_type=origin

openshift_release=v3.9

debug_level=3

# allow htpasswd
openshift_master_identity_providers=[{'name': 'htpasswd', 'login': 'true', 'challenge': 'true', 'kind': 'HTPasswdPasswordIdentityProvider', 'filename': '/root/users.htpasswd'}]
openshift_master_htpasswd_file=/root/users.htpasswd

# openshift_disable_check=disk_availability,memory_availability,docker_image_availability,package_version

openshift_router_selector='region=infra'
openshift_registry_selector='region=infra'

openshift_set_node_ip=true
openshift_public_ip=192.168.56.11
```
Je vais essayer de décrire au mieux ce fichier mais encore une fois n'hésitez pas à consulter [la documentation officielle](https://docs.okd.io/3.9/install_config/install/advanced_install.html).

Nous devons définir quatre groupes : masters, etcd, nodes, glusterfs. Dans les deux premiers groupes il faut spécifier le nom de domaine du master et son IP sur laquelle la console web se déploiera. 
Pour la partie nodes, il faut mettre chaque machines avec leurs IPs et un label qui va permettre de déployer des pods sur un node spécifique. 
Et pour finir dans la section glusterfs on y met chaque machine avec leurs IPs pour se joindre entre elles et le(s) disque(s) utilisé(s) pour GlusterFS. 
Pour la partie [OSEv3:vars], on spécifie plusieurs variables. D'abord on définit que GlusterFS est utilisé par défaut, puis qu'on utilise l'utilisateur root pour ansible. Ensuite on spécifie la type de déploiement, sa version et le debug. On spécifie ensuite le chemin du fichier htpasswd qui servira pour se connecter au master. 
La ligne commentée sert à contourner les vérifications de l'espace disque, RAM ... (A décommenter juste pour faire des tests sur des petites machines. On désigne ensuite dans quelle région seront déployés les pods pour router et registry (d'où l'importance des labels sur les nodes). Et pour finir on spécifie qu'on donne les IPs pour les nodes et on donne également l'IP public où sera déployée la console web.

On créé ensuite le fichier users.htpasswd avec les logins, mots de passes
```
# yum -y install httpd-tools
# htpasswd -c /root/users.htpasswd <user> 
```
On copie ensuite notre fichier de configuration :
```
# cp cluster.conf /etc/ansible/hosts
```
## Installation 
Déploiement du cluster Openshift Origin
```
# ansible-playbook openshift-ansible/playbooks/prerequisites.yml
# ansible-playbook openshift-ansible/playbooks/deploy_cluster.yml
```
### Erreurs connues
- Erreur lors de la tâche [restart master api] : Mettre `openshift_public_ip=<ip>` dans le fichier de configuration avec l'ip où sera déployer la console web sur le master.
- Erreur lors de la tâche [Wait glusterfs pods] : Regarder le pod qui n'a pas démarrer avec `oc get pods --all-namespaces` et regarder ses logs avec `oc describe pods -n <namespace> <pod_name>`
- SI la tâche [Ensure OpenShift pod correctly rolls out (best-effort today)] est très longue cela veut dire que les pods router ou registry se sont pas bien déployés donc faire Ctrl-C pour arrêter ansible et regarder le pod qui n'a pas démarrer avec `oc get pods --all-namespaces` puis regarder ses logs avec `oc describe -n <namespace> <pod_name>`. Il s'agit souvent d'une erreur de node selector. C'est à dire que les restrictions (`openshift_router_selector='region=infra'` ou`openshift_registry_selector='region=infra'`) spécifiées dans le fichier de configfuration ne match avec aucun label d'un node.

**Voilà votre cluster Openshift Origin est donc bien créé et vous pouvez y accéder grâce à la console web (https://<master_ip>:8443) ou en ligne de commande sur le master (`# oc login -u <user>`)**

## Désinstallation
Si vous avez besoin de désinstaller Openshift Origin éxecuter ce playbook
```
# ansible-playbook openshift-ansible/playbooks/adhoc/uninstall.yml
```
## Ajouter un node à un cluster existant
Vous devez bien sur préparer votre nouvelle machine comme expliquer au dessus, puis modifier le fichier de configuration comme ceci (en ajoutant une section new_nodes et l'ajouter dans les enfants de OSEv3) :
```
[masters]
master openshift_ip=192.168.56.11

[etcd]
master

[nodes]
master openshift_node_labels="{'region': 'primary','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.11 openshift_schedulable=true
node01 openshift_node_labels="{'region': 'infra','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.12 openshift_schedulable=true
node02 openshift_node_labels="{'region': 'infra','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.14 openshift_schedulable=true

[new_nodes]
node03 openshift_node_labels="{'region': 'infra','node-role.kubernetes.io/compute': 'true','zone': 'default'}" openshift_ip=192.168.56.15 openshift_schedulable=true

[glusterfs]
master glusterfs_ip=192.168.56.11 glusterfs_devices='["/dev/sdb"]'
node01 glusterfs_ip=192.168.56.12 glusterfs_devices='["/dev/sdb"]'
node02 glusterfs_ip=192.168.56.14 glusterfs_devices='["/dev/sdb"]'

[OSEv3:children]
masters
nodes
etcd
glusterfs
new_nodes

[OSEv3:vars]
openshift_storage_glusterfs_wipe=true
openshift_storage_glusterfs_storageclass=true
openshift_storage_glusterfs_storageclass_default=true

ansible_ssh_user=root

openshift_deployment_type=origin

openshift_release=v3.9

debug_level=3

# allow htpasswd
openshift_master_identity_providers=[{'name': 'htpasswd', 'login': 'true', 'challenge': 'true', 'kind': 'HTPasswdPasswordIdentityProvider', 'filename': '/root/users.htpasswd'}]
openshift_master_htpasswd_file=/root/users.htpasswd

# openshift_disable_check=disk_availability,memory_availability,docker_image_availability,package_version

openshift_router_selector='region=infra'
openshift_registry_selector='region=infra'

openshift_set_node_ip=true
openshift_public_ip=192.168.56.11
```
Executer ensuite le playbook pour ajouter le nouveau node
```
# ansible-playbook openshift-ansible/playbooks/openshift-node/scaleup.yml
```
Pensez ensuite à bien déplacer le nouveau node dans le groupe [nodes] car à la prochaine execution du fichier le node03 ne sera plus considéré comme nouveau.
